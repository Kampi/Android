package com.kampis_elektroecke.customviewexample;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.LinearLayout;

import androidx.annotation.Nullable;

public class CustomView extends LinearLayout
{
    public CustomView(Context context, @Nullable AttributeSet attrs)
    {
        super(context, attrs);
        inflate(context, R.layout.custom_view, this);
    }
}