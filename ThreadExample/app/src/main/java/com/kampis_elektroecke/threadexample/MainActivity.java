package com.kampis_elektroecke.threadexample;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.util.Log;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity
{
    private TextView _mCounterOutput;

    private Thread _mWorkerThread;

    private Handler _mThreadHandler;

    private int _mCounter;

    private Runnable _Worker()
    {
        return () -> {
            Message Result;

            Log.d("MyThread", "Run this thread...");

            try
            {
                // A simple busy loop for the worker thread
                Thread.sleep(10000);
            }
            catch(InterruptedException e)
            {

            }

            // Create the message for the main thread
            Result = _mThreadHandler.obtainMessage();

            // Send the message to the main thread
            Result.sendToTarget();

            Log.d("MyThread", "Thread finished...");
        };
    }

    private void _ThreadFinish()
    {
        Toast.makeText(getApplicationContext(), "Switch to UI thread", Toast.LENGTH_LONG).show();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    @Override
    protected void onResume()
    {
        super.onResume();

        _mCounter = 0;

        _mCounterOutput = findViewById(R.id.TextViewCounter);
        Button Trigger = findViewById(R.id.ButtonTrigger);
        Trigger.setOnClickListener(v ->
            {
                _mCounterOutput.setText("" + _mCounter);
                _mCounter++;
            }
        );

        // Create a new worker thread
        _mWorkerThread = new Thread(_Worker());

        // Create a thread handler to communicate with the main thread from the worker thread
        _mThreadHandler = new Handler(Looper.getMainLooper())
        {
            @Override
            public void handleMessage(Message message)
            {
                _ThreadFinish();
            }
        };

        // Start the worker thread
        _mWorkerThread.start();
    }
}