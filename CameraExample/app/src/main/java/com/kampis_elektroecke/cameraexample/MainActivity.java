package com.kampis_elektroecke.cameraexample;

import android.Manifest;
import android.app.Activity;
import android.content.pm.PackageManager;
import android.graphics.ImageFormat;
import android.hardware.camera2.CameraAccessException;
import android.hardware.camera2.CameraCaptureSession;
import android.hardware.camera2.CameraCharacteristics;
import android.hardware.camera2.CameraDevice;
import android.hardware.camera2.CameraManager;
import android.hardware.camera2.CaptureRequest;
import android.hardware.camera2.TotalCaptureResult;
import android.hardware.camera2.params.StreamConfigurationMap;
import android.media.Image;
import android.media.ImageReader;
import android.os.Bundle;
import android.os.Handler;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.Size;
import android.view.Surface;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends Activity
{
    private String _mCameraID;
    private Size[] _mImageSizes;
    private CameraDevice _mCamera;
    private CameraManager _mCameraManager;
    private CameraCaptureSession _mActiveSession;
    private CaptureRequest.Builder _mCapturePictureBuilder;
    private ImageReader _mImageReader;

    private TextView _mTextView;
    private SurfaceView _mCameraView;
    private SurfaceHolder _mSurfaceHolder;

    private void _createCaptureSession()
    {
        List<Surface> ImageOutputs = new ArrayList<>();

        // Get the surface object from the image reader
        Surface ImageReaderSurface = _mImageReader.getSurface();

        // Image outputs for the camera
        ImageOutputs.add(_mSurfaceHolder.getSurface());
        ImageOutputs.add(ImageReaderSurface);

        try
        {
            // Create the image builder
            _mCapturePictureBuilder = _mCamera.createCaptureRequest(CameraDevice.TEMPLATE_STILL_CAPTURE);
            //_mCapturePictureBuilder.addTarget(ImageReaderSurface);
            _mCapturePictureBuilder.addTarget(_mSurfaceHolder.getSurface());
            _mCamera.createCaptureSession(ImageOutputs, _captureSessionCallback, new Handler());
        }
        catch(Exception e)
        {
            Log.e("MainActivity", "Capture exception:", e);
        }
    }

    private void _configureCamera()
    {
        _mSurfaceHolder = _mCameraView.getHolder();
        _mSurfaceHolder.addCallback(_CameraViewCallbacks);
        _mCameraManager = getSystemService(CameraManager.class);

        // Get the camera and the possible image sizes
        _findCamera();
        if((_mCameraID == null) || (_mImageSizes == null))
        {
            Log.d("MainActivity", "No camera found!");
        }
        else
        {
            boolean found = false;

            // Get the display metrics
            DisplayMetrics Metrics = getResources().getDisplayMetrics();

            // Search the optimal image size for the display
            for(Size ImageSize : _mImageSizes)
            {
                int Width = ImageSize.getWidth();
                int Height = ImageSize.getHeight();
                if((Width > Metrics.widthPixels) || (Height > Metrics.heightPixels))
                {
                    continue;
                }

                _mSurfaceHolder.setFixedSize(Width, Height);
                found = true;

                // Configure the image reader
                _mImageReader = ImageReader.newInstance(Width, Height, ImageFormat.JPEG, 2);
                _mImageReader.setOnImageAvailableListener(_onImageAvailable, null);

                break;
            }

            // No optimal size found
            if(!found)
            {
                Log.d("MainActivity", "Image is too big!");
            }
        }

        _mCameraView.setVisibility(View.VISIBLE);
    }

    private void _findCamera()
    {
        try
        {
            boolean CameraFound = false;

            // Loop over each camera
            String[] CameraIDs = _mCameraManager.getCameraIdList();
            for(String CameraID: CameraIDs)
            {
                // Abort if one camera was found
                if(CameraFound)
                {
                    break;
                }

                // Get the data from the current camera
                CameraCharacteristics Characteristics = _mCameraManager.getCameraCharacteristics(CameraID);
                Log.d("MainActivity", "Camera" + " : " + Characteristics.toString());

                // Get the lens facing direction
                Integer lensFacing = Characteristics.get(CameraCharacteristics.LENS_FACING);
                if((lensFacing != null) && (lensFacing == CameraCharacteristics.LENS_FACING_BACK))
                {
                    CameraFound = true;
                    _mCameraID = CameraID;

                    // Get all possible image sizes
                    StreamConfigurationMap StreamConfigs = Characteristics.get(CameraCharacteristics.SCALER_STREAM_CONFIGURATION_MAP);
                    if(StreamConfigs != null)
                    {
                        _mImageSizes = StreamConfigs.getOutputSizes(SurfaceHolder.class);
                    }
                }
            }
        }
        catch(CameraAccessException | NullPointerException e)
        {
            Log.e("MainActivity", "Camera exception:", e);
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        _mCameraView = findViewById(R.id.CameraView);
        _mTextView = findViewById(R.id.textView);

        _configureCamera();

        Log.d("MainActivity", "onCreate");
    }

    @Override
    protected void onStart()
    {
        super.onStart();

        Log.d("MainActivity", "onStart");
    }

    @Override
    public void onResume()
    {
        super.onResume();

        if (checkSelfPermission(Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED)
        {
            requestPermissions(new String[] {Manifest.permission.CAMERA}, 123);
        }
        else
        {
            _configureCamera();
        }

        Log.d("MainActivity", "onResume");
    }

    @Override
    public void onPause()
    {
        super.onPause();

        // Remove the visibility from the surface view
        _mCameraView.setVisibility(View.GONE);

        if(_mCamera != null)
        {
            // Close the current session
            if(_mActiveSession != null)
            {
                _mActiveSession.close();
                _mActiveSession = null;
            }

            // Close the camera
            _mCamera.close();
            _mCamera = null;

            // Remove the surface holder callbacks
            if(_mSurfaceHolder != null)
            {
                _mSurfaceHolder.removeCallback(_CameraViewCallbacks);
            }
        }

        Log.d("MainActivity", "onPause");
    }

    @Override
    protected void onStop()
    {
        super.onStop();

        Log.d("MainActivity", "onStop");
    }

    @Override
    protected void onDestroy()
    {
        super.onDestroy();

        Log.d("MainActivity", "onDestroy");
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String permissions[], @NonNull int[] grantResults)
    {
        if((requestCode == 123) && (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED))
        {
            _configureCamera();
        }
    }

    /**
     *  Callbacks for the camera view SurfaceHolder view
     */
    private final SurfaceHolder.Callback _CameraViewCallbacks = new SurfaceHolder.Callback()
    {
        // This callback is fired when the view is created.
        @Override
        public void surfaceCreated(SurfaceHolder surfaceHolder)
        {
            // Open the camera when the surface is created
            try
            {
                _mCameraManager.openCamera(_mCameraID, _cameraDeviceCallbacks, null);
            }
            catch(SecurityException | CameraAccessException e)
            {
                Log.e("MainActivity", "Surface exception:", e);
            }

            Log.d("MainActivity", "Surface created");
        }

        // This callback is fired when the view changes.
        @Override
        public void surfaceChanged(SurfaceHolder surfaceHolder, int i, int i1, int i2)
        {
            Log.d("MainActivity", "Surface changed");
        }

        // This callback is fired when the view is destroyed.
        @Override
        public void surfaceDestroyed(SurfaceHolder surfaceHolder)
        {
            Log.d("MainActivity", "Surface destroyed");
        }
    };

    /**
     *  Callbacks for the state of the camera device.
     */
    private final CameraDevice.StateCallback _cameraDeviceCallbacks = new CameraDevice.StateCallback()
    {
        // This callback is fired when the camera has finished opening.
        @Override
        public void onOpened(@NonNull CameraDevice cameraDevice)
        {
            _mCamera = cameraDevice;
            _createCaptureSession();

            Log.d("MainActivity", "Camera onOpened");
        }

        // This callback is used when the camera device has been closed.
        @Override
        public void onClosed(@NonNull CameraDevice cameraDevice)
        {
        }

        // This callback is fired when the camera device is no longer available for use.
        @Override
        public void onDisconnected(@NonNull CameraDevice cameraDevice)
        {
            Log.d("MainActivity", "Camera onDisconnected");
        }

        // This callback is fired when the camera has encountered a serious error.
        @Override
        public void onError(@NonNull CameraDevice cameraDevice, int i)
        {
            Log.d("MainActivity", "Camera onError: " + i);
        }
    };

    /**
     *  Callbacks for the status of the camera capture session.
     */
    private final CameraCaptureSession.StateCallback _captureSessionCallback = new CameraCaptureSession.StateCallback()
    {
        // This callback is fired when the camera device has finished configuring, and the session can start processing capture requests.
        @Override
        public void onConfigured(CameraCaptureSession cameraCaptureSession)
        {
            try
            {
                cameraCaptureSession.setRepeatingRequest(_mCapturePictureBuilder.build(), _captureCallback, null);
                _mActiveSession = cameraCaptureSession;
            }
            catch(CameraAccessException e)
            {
                Log.e("MainActivity", "Camera access exception:", e);
            }
        }

        // This callback is fired when the session cannot be configured.
        @Override
        public void onConfigureFailed(@NonNull CameraCaptureSession cameraCaptureSession)
        {
            Log.e("MainActivity", "Session configuration failed!:");
        }
    };

    /**
     *  Callbacks for the progress of the capture request of the camera.
     */
    private final CameraCaptureSession.CaptureCallback _captureCallback = new CameraCaptureSession.CaptureCallback()
    {
        // This callback is fired when an image capture has fully completed.
        @Override
        public void onCaptureCompleted(@NonNull CameraCaptureSession session, @NonNull CaptureRequest request, @NonNull TotalCaptureResult result)
        {
            super.onCaptureCompleted(session, request, result);

            _mTextView.setText(getString(R.string.frameNumber, result.getFrameNumber()));
        }
    };

    /**
     *  Callbacks for the camera image reader.
     */
    private final ImageReader.OnImageAvailableListener _onImageAvailable = new ImageReader.OnImageAvailableListener()
    {
        // This callback is fired when the image reader receives a new image.
        @Override
        public void onImageAvailable(ImageReader imageReader)
        {
            Log.d("MainActivity", "Image");

            Image image = imageReader.acquireLatestImage();
            image.close();
        }
    };
}